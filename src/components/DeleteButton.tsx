"use client";

import React from "react";
import { Button } from "./ui/button";
import { Trash } from "lucide-react";
import { useMutation } from "@tanstack/react-query";
import axios from "axios";
import { useRouter } from "next/navigation";

type Props = {
  noteId: number;
};

const DeleteButton = ({ noteId }: Props) => {
  const router = useRouter();
  const deleteJournal = useMutation({
    mutationFn: async () => {
      const response = await axios.post("/api/deleteNote", {
        noteId,
      });
      return response.data;
    },
  });

  const handleDeleteJournal = () => {
    const confirm = window.confirm(
      "Are you sure you want to delete this note?"
    );
    if (!confirm) {
      return;
    }
    deleteJournal.mutate(undefined, {
      onSuccess: () => {
        router.push("/dashboard");
      },
      onError: (error) => {
        console.error(error);
      },
    });
  };

  return (
    <Button onClick={handleDeleteJournal} variant="destructive" size="sm">
      <Trash /> <span>Delete</span>
    </Button>
  );
};

export default DeleteButton;

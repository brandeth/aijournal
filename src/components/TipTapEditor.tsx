"use client";

import React, { useEffect, useRef } from "react";
import { EditorContent, useEditor } from "@tiptap/react";
import { StarterKit } from "@tiptap/starter-kit";
import TipTapMenuBar from "./TipTapMenuBar";
import { Button } from "./ui/button";
import { useDebounce } from "@/lib/useDebounce";
import { useMutation } from "@tanstack/react-query";
import Text from "@tiptap/extension-text";
import axios from "axios";
import { NoteType } from "@/lib/db/schema";
import { useCompletion } from "ai/react";

type Props = {
  note: NoteType;
};

const TipTapEditor = ({ note }: Props) => {
  const [editorState, setEditorState] = React.useState<any>(
    note.editorState || `<h1>${note.name}</h1>`
  );
  const { complete, completion } = useCompletion({
    api: "/api/completion",
  });
  const saveNote = useMutation({
    mutationFn: async () => {
      const response = await axios.post("/api/saveJournal", {
        noteId: note.id,
        editorState,
      });

      return response.data;
    },
  });
  const customText = Text.extend({
    addKeyboardShortcuts() {
      return {
        "Shift-a": () => {
          const prompt = this.editor.getText().split(" ").slice(-30).join(" ");
          complete(prompt);
          return true;
        },
      };
    },
  });
  const editor = useEditor({
    extensions: [StarterKit, customText],
    content: editorState,
    onUpdate: ({ editor }) => {
      setEditorState(editor.getHTML());
    },
  });
  const debouncedEditorState = useDebounce(editorState, 500);
  const lastCompletion = useRef("");

  useEffect(() => {
    if (debouncedEditorState === "") return;
    saveNote.mutate(undefined, {
      onSuccess: (data) => {
        console.log("success update", data);
      },
      onError: (error) => {
        console.error(error);
      },
    });
  }, [debouncedEditorState]);

  useEffect(() => {
    if (!completion || !editor) return;
    const diff = completion.slice(lastCompletion.current.length);
    lastCompletion.current = completion;
    editor?.commands.insertContent(diff);
  }, [completion, editor]);

  setTimeout(() => {
    editor?.commands.focus();
  }, 300);

  return (
    <>
      {editor && (
        <>
          <div className="flex items-center justify-between mb-4">
            <TipTapMenuBar editor={editor} />
            <Button disabled variant="outline" size="sm">
              {saveNote.isPending ? "Saving..." : "Saved"}
            </Button>
          </div>
          <div className="prose prose-sm w-full my-4">
            <EditorContent editor={editor} />
          </div>
          <span className="text-sm">
            Tip: Press{" "}
            <kbd className="px-2 py-1.5 text-xs font-semibold text-gray-800 bg-gray-100 border border-gray-200 rounded-lg">
              Shift + A
            </kbd>{" "}
            for AI auto completion.
          </span>
        </>
      )}
    </>
  );
};

export default TipTapEditor;

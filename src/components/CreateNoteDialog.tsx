"use client";

import React from "react";
import axios from "axios";
import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "./ui/dialog";
import { Loader2, Plus } from "lucide-react";
import { Input } from "./ui/input";
import { Button } from "./ui/button";
import { useMutation } from "@tanstack/react-query";
import { useRouter } from "next/navigation";

type Props = {};

const CreateNoteDialog = (props: Props) => {
  const router = useRouter();
  const [input, setInput] = React.useState("");
  const uploadToFirebase = useMutation({
    mutationFn: async (noteId: string) => {
      const response = await axios.post("/api/uploadToFirebase", {
        noteId,
      });
      return response.data;
    },
  });
  const createJournal = useMutation({
    mutationFn: async () => {
      const response = await axios.post("/api/createJournal", {
        name: input,
      });
      return response.data;
    },
    onSuccess: () => {},
  });

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (input === "") {
      window.alert("Please enter a title for your journal");
      return;
    }
    createJournal.mutate(undefined, {
      onSuccess: ({ note_id }) => {
        setInput("");
        console.log(`Created new journal with id ${note_id}`);
        uploadToFirebase.mutate(note_id);
        router.push(`/journal/${note_id}`);
      },
      onError: (error) => {
        console.error(error);
        window.alert(
          "An error occurred while creating your journal. Please try again."
        );
      },
    });
  };

  return (
    <Dialog>
      <DialogTrigger>
        <div className="border-dashed border-2 border-green-600 h-full rounded-lg flex items-center justify-center sm:flex-col hover:shadow-xl transition hover:-translate-y-1 flex-row p-4">
          <Plus className="w-6 h-6 text-green-600" strokeWidth={3} />
          <h2 className="font-semibold text-green-600 sm:mt-2">New Journal</h2>
        </div>
      </DialogTrigger>

      <DialogContent>
        <DialogHeader>
          <DialogTitle>New Journal</DialogTitle>
          <DialogDescription>
            Create your first journal entry by clicking the button below.
          </DialogDescription>
        </DialogHeader>
        <form onSubmit={handleSubmit} className="flex flex-col gap-4 py-4">
          <Input
            value={input}
            onChange={(e) => setInput(e.target.value)}
            placeholder="Title..."
          />
          <div className="flex items-center gap-2 mt-2">
            <Button type="reset" variant={"secondary"}>
              Cancel
            </Button>
            <Button
              type="submit"
              className="bg-green-600 flex flex-items gap-2"
              disabled={createJournal.isPending}
            >
              {createJournal.isPending && (
                <Loader2 className="w-4 h-4 animate-spin" />
              )}
              Create
            </Button>
          </div>
        </form>
      </DialogContent>
    </Dialog>
  );
};

export default CreateNoteDialog;

"use client";

import React from "react";

import GraphemeSplitter from "grapheme-splitter";
import Typewriter from "typewriter-effect";

type Props = {};

const stringSplitter = (text: string): string | any => {
  const splitter = new GraphemeSplitter();
  return splitter.splitGraphemes(text);
};

const TypeWriterTitle = (props: Props) => {
  return (
    <Typewriter
      options={{
        loop: true,
        delay: 75,
        stringSplitter,
      }}
      onInit={(typewriter) => {
        typewriter
          .typeString("🚀 Supercharged Productivity")
          .pauseFor(1000)
          .deleteAll()
          .typeString("🤖 Take Notes With AI Autocompletion")
          .pauseFor(1000)
          .deleteAll()
          .typeString("🔍 Search Your Notes With AI")
          .pauseFor(1000)
          .start();
      }}
    ></Typewriter>
  );
};

export default TypeWriterTitle;

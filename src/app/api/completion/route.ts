import { OpenAIStream, StreamingTextResponse } from "ai";
import { OpenAIApi, Configuration } from "openai-edge";

const config = new Configuration({
  apiKey: process.env.OPENAI_API_KEY,
});

const openai = new OpenAIApi(config);

export async function POST(req: Request) {
  const { prompt } = await req.json();
  const response = await openai.createChatCompletion({
    model: "gpt-4",
    messages: [
      {
        role: "system",
        content:
          "Welcome to your personal journal assistant. I am designed to be expert, helpful, and clever, always behaving and responding in a well-mannered, friendly, and kind manner. I'm here to inspire you and eagerly await to provide assistance with your journaling needs. How may I assist you today?",
      },
      {
        role: "user",
        content: `Help me complete my stream of consiousness based on the ff: ${prompt}. Keep the tone positive, encouraging and consistent with the rest of the text. Keep the response concise and sweet.`,
      },
    ],
    stream: true,
  });

  const stream = OpenAIStream(response);

  return new StreamingTextResponse(stream);
}

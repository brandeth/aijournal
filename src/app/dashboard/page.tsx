import React from "react";
import Link from "next/link";
import { Button } from "@/components/ui/button";
import { ArrowLeft } from "lucide-react";
import { UserButton, auth } from "@clerk/nextjs";
import { Separator } from "@/components/ui/separator";
import CreateNoteDialog from "@/components/CreateNoteDialog";
import { db } from "@/lib/db";
import { $notes } from "@/lib/db/schema";
import { eq } from "drizzle-orm";
import Image from "next/image";

type Props = {};

const DashboardPage = async (props: Props) => {
  const { userId } = auth();
  const notes = await db
    .select()
    .from($notes)
    .where(eq($notes.userId, userId!));

  return (
    <>
      <div className="grainy min-h-screen">
        <div className="max-w-7xl mx-auto p-10">
          <div className="flex justify-between items-center md:flex-row flex-col">
            <div className="flex items-center gap-4">
              <Link href="/">
                <Button
                  className="flex items-center gap-2 bg-green-600"
                  size="sm"
                >
                  <ArrowLeft className="h-5 w-5" strokeWidth={3} />{" "}
                  <span>Back</span>
                </Button>
              </Link>
              <h1 className="text-3xl font-bold text-gray-900">My Journals</h1>
              <UserButton />
            </div>
          </div>

          <Separator className="my-8" />

          {notes.length === 0 && (
            <div className="text-center">
              <h2 className="text-xl text-gray-500">
                You have no journals yet.
              </h2>
            </div>
          )}

          <div className="grid sm:grid-cols-3 md:grid-cols-5 grid-cols-1 gap-3 mt-6">
            <CreateNoteDialog />

            {notes.map((note) => (
              <Link href={`/journal/${note.id}`} key={note.id}>
                <div className="rounded-lg border border-stone-200 overflow-hidden flex flex-col hover:shadow-xl transition hover:-translate-y-1">
                  <Image
                    width={400}
                    height={200}
                    src={`${note.imageUrl}` || ""}
                    alt={note.name}
                  />
                  <div className="p-4">
                    <h3 className="mb-1 text-xl font-semibold text-gray-900">
                      {note.name}
                    </h3>
                    <p className="text-sm text-gray-500">
                      {new Date(note.createdAt).toLocaleDateString()}
                    </p>
                  </div>
                </div>
              </Link>
            ))}
          </div>
        </div>
      </div>
    </>
  );
};

export default DashboardPage;

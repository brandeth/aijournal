import { Configuration, OpenAIApi } from "openai-edge";

const config = new Configuration({
  apiKey: process.env.OPENAI_API_KEY,
});

const openai = new OpenAIApi(config);

export async function generateImagePrompt(name: string) {
  try {
    const response = await openai.createChatCompletion({
      model: "gpt-4",
      messages: [
        {
          role: "system",
          content:
            "As a creative and resourceful AI assistant, I am adept at crafting unique and engaging thumbnail descriptions for your journal. The descriptions I provide will be succinct, stylized, and inventive, ensuring they are well-suited for input into the DALL-E API for thumbnail image generation.",
        },
        {
          role: "user",
          content: `Generate thumbnail description for: ${name}`,
        },
      ],
    });

    const data = await response.json();
    const imageDescription = data.choices[0].message.content;

    return imageDescription as string;
  } catch (error) {
    console.log(error);
    throw error;
  }
}

export async function generateImage(imageDescription: string) {
  try {
    const response = await openai.createImage({
      prompt: imageDescription,
      n: 1,
      size: "256x256",
    });
    const data = await response.json();
    const imageUrl = data.data[0].url;
    return imageUrl as string;
  } catch (error) {
    console.error(error);
  }
}
